USE [master]
GO
/****** Object:  Database [AgendaDB]    Script Date: 11/5/2020 9:27:47 p. m. ******/
CREATE DATABASE [AgendaDB]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'AgendaDB', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.SQLEXPRESS\MSSQL\DATA\AgendaDB.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'AgendaDB_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.SQLEXPRESS\MSSQL\DATA\AgendaDB_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
GO
ALTER DATABASE [AgendaDB] SET COMPATIBILITY_LEVEL = 140
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [AgendaDB].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [AgendaDB] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [AgendaDB] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [AgendaDB] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [AgendaDB] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [AgendaDB] SET ARITHABORT OFF 
GO
ALTER DATABASE [AgendaDB] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [AgendaDB] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [AgendaDB] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [AgendaDB] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [AgendaDB] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [AgendaDB] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [AgendaDB] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [AgendaDB] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [AgendaDB] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [AgendaDB] SET  DISABLE_BROKER 
GO
ALTER DATABASE [AgendaDB] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [AgendaDB] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [AgendaDB] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [AgendaDB] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [AgendaDB] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [AgendaDB] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [AgendaDB] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [AgendaDB] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [AgendaDB] SET  MULTI_USER 
GO
ALTER DATABASE [AgendaDB] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [AgendaDB] SET DB_CHAINING OFF 
GO
ALTER DATABASE [AgendaDB] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [AgendaDB] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [AgendaDB] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [AgendaDB] SET QUERY_STORE = OFF
GO
USE [AgendaDB]
GO
/****** Object:  Table [dbo].[Contacto]    Script Date: 11/5/2020 9:27:47 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Contacto](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[nombre] [nvarchar](50) NULL,
	[telefono] [nvarchar](50) NULL,
	[direccion] [nvarchar](50) NULL,
	[email] [nvarchar](50) NULL,
	[estado] [nvarchar](50) NULL
) ON [PRIMARY]
GO
/****** Object:  StoredProcedure [dbo].[SP_DeleteContacto]    Script Date: 11/5/2020 9:27:48 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Author:		<Chakyll Calderon>
-- Create date: <11/05/2020>
-- Description:	<Delete Contacto> 
CREATE PROCEDURE [dbo].[SP_DeleteContacto]
@id INT
AS
UPDATE [dbo].[Contacto] SET estado = 0
WHERE id = @id

GO
/****** Object:  StoredProcedure [dbo].[SP_InsertUpdateContacto]    Script Date: 11/5/2020 9:27:48 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Author:		<Chakyll Calderon>
-- Create date: <11/05/2020>
-- Description:	<InsertUpdate Contacto> 
CREATE PROCEDURE [dbo].[SP_InsertUpdateContacto]
@id int,
@nombre NVARCHAR(50),
@telefono NVARCHAR(50),
@direccion NVARCHAR(50),
@email NVARCHAR(50)
AS
if(EXISTS(SELECT * FROM Contacto WHERE id=@id))
BEGIN
UPDATE [dbo].[Contacto] SET nombre = @nombre , telefono = @telefono, direccion = @direccion, email = @email
WHERE id = @id
END
ELSE
BEGIN
INSERT INTO [dbo].[Contacto] (nombre,telefono,direccion,email,estado)
VALUES (@nombre,@telefono,@direccion,@email,1)
END
GO
/****** Object:  StoredProcedure [dbo].[SP_SelectContacto]    Script Date: 11/5/2020 9:27:48 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Author:		<Chakyll Calderon>
-- Create date: <11/05/2020>
-- Description:	<SP que permite seleccionar todos los contactos dentro de la tabla> 
CREATE PROCEDURE [dbo].[SP_SelectContacto]
@estado BIT
AS
SELECT *
FROM [dbo].[Contacto]
WHERE estado = 1
GO
USE [master]
GO
ALTER DATABASE [AgendaDB] SET  READ_WRITE 
GO
