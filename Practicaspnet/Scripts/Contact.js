﻿$("#add").on("click", () => {
    var nombre = $("#nombre_contacto").val();
    var telefono = $("#telefono_contacto").val();
    var direccion = $("#direccion_contacto").val();
    var email = $("#email_contacto").val();
    var contacto = { id: "-1", nombre: nombre, telefono: telefono, direccion: direccion, email: email };
    $.ajax({
        method: "POST",
        url: "/Contacto/CrearContacto",
        data: contacto
    })
        .done(function (msg) {
            if (msg == "ok") {
                // Display an info toast with no title
                toastr.success('El contacto ha sido guardado con exito.', 'Contacto')
                //alert(""); Alert sirve para ventana del navegador
                //toastr es para ventana dentro de la misma pagina
            }
        });