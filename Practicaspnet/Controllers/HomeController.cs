﻿using Practicaspnet.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Practicaspnet.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            List<Contacto> contactos = Contacto.ListadoContactos();
            return View(contactos);
        }
       [HttpPost]
        public ActionResult CrearContacto(Contacto contacto)
        {
            try
            {
               Contacto.InsertUpdateContacto(contacto);
               return Json("ok");
            }
            catch (Exception ex)
            {
                return Json("error");
            }

        }
    }
}