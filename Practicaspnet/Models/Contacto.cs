﻿using Practicaspnet.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Practicaspnet.Models
{
    public class Contacto
    {
        [Required]
        public int Id { get; set; }
        [Required]
        public string Nombre { get; set; }
        [Required]
        public string Telefono { get; set; }
        public string Direccion { get; set; }
        public string Email { get; set; }
        public bool Estado { get; set; }


        public Contacto(int id, string nombre, string telefono, string direccion, string correo, bool estado)
        {
            this.Id = id;
            this.Nombre = nombre;
            this.Telefono = telefono;
            this.Direccion = direccion;
            this.Email = correo;
            this.Estado = estado;
        }
        public Contacto()
        {

        }
        public static List<Contacto> ListadoContactos()
        {
            //Para poder usar tu base de datos, todos los que digan AdminDS2 deben ser cambiados a AdminDS1
            Practicaspnet.Data.AdminDS2TableAdapters.ContactoTableAdapter DA = new Practicaspnet.Data.AdminDS2TableAdapters.ContactoTableAdapter();
            AdminDS2.ContactoDataTable DT = DA.SelectContacto(true);
            List<Contacto> listContacto = new List<Contacto>();
            foreach (AdminDS2.ContactoRow dr in DT.Rows)
            {
                listContacto.Add(new Contacto(dr.id, dr.nombre, dr.telefono, dr.direccion, dr.email, dr.estado));
            }
            return listContacto;
        }
        public static List<Contacto> DetalleContacto()
        {
            //Para poder usar tu base de datos, todos los que digan AdminDS2 deben ser cambiados a AdminDS1
            Practicaspnet.Data.AdminDS2TableAdapters.ContactoTableAdapter DA = new Practicaspnet.Data.AdminDS2TableAdapters.ContactoTableAdapter();
            AdminDS2.ContactoDataTable DT = DA.SelectContacto(true);
            List<Contacto> listContacto = new List<Contacto>();
            foreach (AdminDS2.ContactoRow dr in DT.Rows)
            {
                listContacto.Add(new Contacto(dr.id, dr.nombre, dr.telefono, dr.direccion, dr.email, dr.estado));
            }
            return listContacto;
        }
        //Este es el metodo en el modelo por el cual hacemos el insert y el update, es casi identico a la logica de arriba solo que se selecciona el SP de insert update.
        public static void InsertUpdateContacto(Contacto contacto)
        {
            Practicaspnet.Data.AdminDS2TableAdapters.ContactoTableAdapter DA = new Practicaspnet.Data.AdminDS2TableAdapters.ContactoTableAdapter();
            DA.InsertUpdateContacto(contacto.Id, contacto.Nombre, contacto.Telefono, contacto.Direccion, contacto.Email);
        }
        public static void DeleteContacto(Contacto contacto)
        {
            Practicaspnet.Data.AdminDS2TableAdapters.ContactoTableAdapter DA = new Practicaspnet.Data.AdminDS2TableAdapters.ContactoTableAdapter();
            DA.DeleteContacto(contacto.Id);
        }
    }
}