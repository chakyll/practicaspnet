﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Practicaspnet.Models
{
    public class Baltri
    {
        public int BaltriId { get; set; }
        public string Nombre { get; set; }
        public string Apellido { get; set; }
        public int Edad { get; set; }
    }
}